import {Component, OnInit} from '@angular/core';
import axios, {AxiosResponse} from "axios"
import {Group} from "./group";
import {logger} from "codelyzer/util/logger";
import {HttpClient} from "@angular/common/http";
import {GroupService} from "./group.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'front';
  groupList: Group[] ;



  constructor(private groupService: GroupService) {
    this.groupList = []
  }

  ngOnInit(): void {
    this.getGroups();
  }

  getGroups(): void {
    this.groupService.getGroups().subscribe((groups: Group[]) => {
      this.groupList = groups
    });
  }
}
