export class Group {
  id: number;
  name: string;
  coverage: number;
  updatedAt: string;


  constructor(id: number, name: string, coverage: number, updatedAt: string ) {
    this.id = id;
    this.name = name;
    this.coverage = coverage;
    this.updatedAt = updatedAt;
  }
}
