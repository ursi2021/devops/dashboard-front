import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Group} from "./group";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  private groupUrl = 'http://localhost:10000/';  // URL to web api

  constructor(private http: HttpClient) { }

  getGroups() : Observable<Group[]>
  {
      return this.http.get<Group[]>(this.groupUrl);
  }
}
